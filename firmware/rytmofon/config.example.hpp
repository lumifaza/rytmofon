const int LED_PIN = LED_BUILTIN;
const int UNLOCK_PIN = 4;
const int AUDIO_OUT_PIN = 5;

// code input time tolerance configuration:
#define MARGIN_FACTOR 3 / 8
const int MARGIN_MAX_MS = 250;

///////////////////////////////////////////////////////////////////////////////
// secret rhythm code definition:

// example rhythm:
//  4 quarter notes (any articulation) followed by 2 half notes (50% duty cycle)

// example signals that will be treated as a valid code:
// |#   #   #   #   ####    ####    |
// |01  23  45  67  8   9   10  11  | < indices

// |### #   ##  ##  ####    ####    |
// |0  123  4 5 6 7 8   9   10  11  |
// because we don't check indices 1, 3, 5 & 7, it is also a valid input

// example signals that won't be accepted:
// |#     # #   #   ####### ####    |
// |01    2345  67  8      9/10 11  |
//        ^too late     ^^^too long

// |#   #   #   #   ####    #       |
// |01  23  45  67  8   9   10/11   |
//                          ^ too short

const int CODE_LEN = 12;
const int RB_LEN = 16; // must be 2^N >= CODE_LEN (N = positive integer)

// base delay: in this example duration of the quarter note:
#define CALC_BASE_DELAY (rbread(8)-rbread(0)) / 4

// check condition. bd is the base delay
#define CHECK_CODE \
        c(0, 2, bd) && c(2, 4, bd) && c(4, 6, bd) && c(6, 8, bd) && \
        c(8, 9, bd) && c(8, 10, bd*2) && c(10, 11, bd)

// The last argument of the c(check) function is the correct delay.
// Its tolerance is specified by MARGIN_FACTOR and MARGIN_MAX_MS:
//  margin = (correct_delay * MARGIN_FACTOR) clamped to MARGIN_MAX_MS
// c returns true if:
//  (correct_delay - margin) < actual_delay < (correct_delay + margin)
// It means that longer correct delay values will result in more tolerance
// when checking that delay.

///////////////////////////////////////////////////////////////////////////////
// audio output configuration:

const int BASE_PERIOD = 1953;
const int PWM_EFX_SPEED = 1;
const int PWM_EFX_PRESCALER = 1;

#define PLAY_WELCOME_MELODY \
        playTone(BASE_PERIOD,     300); \
        playTone(BASE_PERIOD*2/3, 300); \
        playTone(BASE_PERIOD*4/5, 300); \
        playTone(BASE_PERIOD/2,   300); \
        playTone(BASE_PERIOD*2/3, 300); \
        playTone(BASE_PERIOD*2/5, 300); \
        playTone(BASE_PERIOD*2/5, 3200, true);

// The door will be locked immediately after the melody is played.
// (use delay(...) to keep the door open without playing)
// The whole melody can't be longer than ~8s - otherwise watchdog will reset the MCU.
// playTone invocation:
//   playTone(tone_period_in_microseconds, tone_length_in_milliseconds, do_pwm_effect = false)
