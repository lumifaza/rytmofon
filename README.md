# Rytmofon (rhythm-o-phone)

Code opening for an analog door phone!

Rytmofon is an accessory for the indoor unit of an analog door phone, 4+1n system with DC strike. Door is opened automatically after the user taps the correct rhythm using the call button on the outdoor plate.

Powered using phantom power normally used for microphone - should fit inside the indoor unit - no external wires or holes in case needed!

## Building
Before attempting to build it, follow the Installation section to check whether it is compatible with your wiring.

![schematic](https://akceptacja.ams3.cdn.digitaloceanspaces.com/ee/rytmofon_2.0_schematic.png)

I've added power regulator (7805) with diodes to reduce voltage on the MCU (to make it draw less current) because for some reason voltage regulator on Arduino board stopped working pretty quickly when connected to a bench power supply.

Piezoelectric buzzer (without oscillator) can be connected between AUDIO_OUT pin and ground to play the door open melody in your apartment.

This is built prototype of Rytmofon 2.0:

![photo of Rytmofon 2.0](https://akceptacja.ams3.cdn.digitaloceanspaces.com/ee/rytmofon_2.0_built_720.jpg)

## Programming
Open project from `firmware/` directory in Arduino IDE.

The correct rhythm needs to be specified in `config.hpp`. Copy `config.example.hpp` to `config.hpp` and change the rhythm inside.

Compile and upload to the Arduino.

## Installation
Reading all the instructions carefully reduces the chance of breaking something, but there is always a risk of damaging this circuit or devices of the door phone system. This instruction may contain errors, may not consider every possible wiring system, or you can make a mistake following it. THIS INSTRUCTION IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY.

1. Find appropriate wires in your wiring. Sometimes functions of wires are described on indoor unit's PCB. If they are not, try to find the manual online.
2. Check that voltage on microphone wire isn't greater than voltage regulator's maximum allowed input - in case of 7805 - 35V. (btw it should be around 12V)
3. Usually the electric strike (door open electromagnet) uses common ground with the audio intercom. If it doesn't, it is not a 4+1n system and you'll need to figure it out yourself (e.g. add a relay)
4. Ensure that door open wire has DC voltage on itself, not AC (relative to ground wire). How to check it? If it is AC, a multimeter on DC range will show fluctuating values or 0. Or use an oscilloscope. If it is AC, again, you'll need to figure it out yourself and, again, a relay will help.
5. Short the door open wire to the ground using an ampmeter set to 10A range. Ensure that the current is less than door open transistor's collector-emitter maximum current. Some strikes may have higher inrush current - if you are afraid of frying the transistor, use an oscilloscope connected to shunt (small value resistor e.g. 0.1ohm) to see momentary current. If the current is too high, add a relay or use a bigger transistor and/or a different type of transistor (e.g. FET).
6. Install the Rytmofon. Start with connecting the ground wire, then the order doesn't matter.
7. Ensure that all functions of the door phone still work and audio quality and volume from indoor handset's microphone, heard in the outdoor plate, didn't suffer. If it did, do not leave the circuit connected - it probably took down the intercom in all apartments!

## Version history
* 1.0 - *schematic never drawn, source code will be shared on request* - used separate power supply and acoustic detection of ringing (microphone & primitive op-amp-based volume meter). Was sent to and published in Szkoła Konstruktorów (*School of Builders*) of the Polish magazine [Elektronika dla Wszystkich](https://avt.pl/portfolio/elektronika-i-automatyka/49-elektronika-dla-wszystkich) (*Electronics for Everyone*)
* 2.0 - **current version**
* 2.1 - *planned*
  * change voltage regulator to have 3.3V output
  * add power-keying transistor to make the whole circuit draw almost no current (just the leakage current of switching transistor) when not in use. Voltage on the call wire will close the key and the MCU will sustain it for a few seconds necessary to enter the code. This is necessary because, in my installation, voltage on the microphone wire drops significantly, impairing amplification.


## License
This README file and photographs are released into the public domain, and WITHOUT WARRANTY OF ANY KIND, under the [CC0 1.0 license](https://creativecommons.org/publicdomain/zero/1.0/).

For licenses of firmware and hardware design (schematic), see LICENSE files in corresponding directories. (they're free & open)
