/*
   Copyright 2023-2024 Teodor Wozniak

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <LowPower.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>

#include "config.hpp"


volatile bool led_enabled = false;

void setOpen(bool state) {
  digitalWrite(UNLOCK_PIN, state);
}

byte sw_pwm_ctr = 0;
SIGNAL(TIMER0_COMPA_vect) {
  digitalWrite(LED_PIN, led_enabled && ((sw_pwm_ctr&7) == 0));
  sw_pwm_ctr++;
}

void setLED(bool light) {
  led_enabled = light;
}

bool input() {
  return !(ACSR & (1<<ACO));
}

void greenSleep(period_t period) {
  LowPower.idle(period, ADC_ON, TIMER2_OFF,
    TIMER1_OFF, TIMER0_ON,
    SPI_OFF, USART0_OFF, TWI_OFF);
}

ISR (ANALOG_COMP_vect) {
  //setLED(input());
}

byte rb_write_pos = 0;
unsigned long rb[RB_LEN] = {0};
bool prev_input = false;

unsigned long rbread(byte index) {
  index += rb_write_pos + (RB_LEN - CODE_LEN);
  index %= RB_LEN;
  return rb[index];
}

// delay Check function
bool c(int i1, int i2, unsigned int dly) {
  if ((rbread(i1)==0) || (rbread(i2)==0) || (dly==0)) return false;
  unsigned long v = rbread(i2)-rbread(i1);
  unsigned long margin = dly * MARGIN_FACTOR;
  if (margin > MARGIN_MAX_MS) margin = MARGIN_MAX_MS;
  if ( (v > (dly-margin)) && (v < (dly+margin)) ) return true;
  return false;
}

void playTone(unsigned int period_us, unsigned int duration_ms, bool pwm_efx = false) {
  unsigned long end_at = millis() + duration_ms;
  unsigned int threshold = period_us / 2;
  signed char pwm_delta = PWM_EFX_SPEED;
  byte pwm_prescaler = 0;
  unsigned long cycle_start = micros();
  do {
    setLED(millis() & 0x80);
    unsigned long cycle_end = cycle_start + period_us;
    unsigned long cycle_invert = cycle_start + threshold;
    digitalWrite(AUDIO_OUT_PIN, 1);
    while (!((cycle_invert-micros()) & 0x80000000)) {};
    digitalWrite(AUDIO_OUT_PIN, 0);
    while (!((cycle_end-micros()) & 0x80000000)) {};
    cycle_start = cycle_end;
    if (pwm_efx) {
      pwm_prescaler++;
      if (pwm_prescaler >= PWM_EFX_PRESCALER) {
        pwm_prescaler = 0;
        threshold += pwm_delta;
        if (threshold > (period_us-50)) {
          pwm_delta = -PWM_EFX_SPEED;
        } else if (threshold < 50) {
          pwm_delta = PWM_EFX_SPEED;
        }
      }
    }
  } while (!((end_at-millis()) & 0x80000000));
}

// the setup routine runs once when you press reset:
void setup() {
  MCUSR = 0;
  wdt_disable();

  DIDR1 = (1<<AIN1D) | (1<<AIN0D); // Digital Input Disable on analog comparator pins
  ADCSRB &= ~(1<<ACME);
  ACSR = (1<<ACBG) | (1<<ACI) | (1<<ACIE);

  OCR0A = 0x7F;
  TIMSK0 |= 1<<OCIE0A;

  pinMode(UNLOCK_PIN, OUTPUT);
  pinMode(AUDIO_OUT_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  setOpen(false);
  digitalWrite(AUDIO_OUT_PIN, 0);

  //playTone(1000, 125);
  setLED(true);
  greenSleep(SLEEP_250MS);
  setLED(false);
}

void loop() {
  wdt_enable(WDTO_8S);
  setOpen(false);
  bool cur_input = input();
  if (cur_input != prev_input) {
    unsigned long end_at = millis() + 33;
    byte ones = 0;
    byte zeros = 0;
    do {
      unsigned long measure_ms = millis();
      if (input()) {
        ones++;
      } else {
        zeros++;
      }
      while (measure_ms == millis()) {};
    } while (!((end_at-millis()) & 0x80000000));
    bool elected_state = ones > (zeros/8);
    if (elected_state == cur_input) {
      prev_input = cur_input;
      setLED(cur_input);
      unsigned long now = millis();
      if (now==0) now = 1;
      rb[rb_write_pos++] = now;
      rb_write_pos %= RB_LEN;
      unsigned long bd = CALC_BASE_DELAY; // Base Delay
      if (CHECK_CODE) {
        // code OK
        for (byte i=0; i<RB_LEN; i++) rb[i] = 0;
        setOpen(true);
        PLAY_WELCOME_MELODY
        setOpen(false);
        setLED(false);
        digitalWrite(AUDIO_OUT_PIN, 0);
      }
    } else {
      return;
    }
  }
  setOpen(false);
  wdt_disable();
  greenSleep(SLEEP_FOREVER);
}
